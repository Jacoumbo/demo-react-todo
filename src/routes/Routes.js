import React from 'react';
import {Switch, Redirect} from 'react-router-dom'
import {useSelector} from "react-redux";
import ConnexionPage from "../pages/Connexion/ConnexionPage";
import InscriptionPage from "../pages/Inscription/InscriptionPage";
import AuthenticatedRoutes from "../stores/AuthenticatedRoutes";
import UnhautenticatedRoutes from "../stores/UnhautenticatedRoutes";

export default function Routes(){

    const user = useSelector(
        (store)=>{
            return store.auth.user
        }
    )

    return (

        <Switch>

            {
                !user && (
                    <UnhautenticatedRoutes />
                )
            }

            {
                !!user && (
                    <AuthenticatedRoutes />
                )
            }

            <Redirect to={'/connexion'} />

        </Switch>

    );
}