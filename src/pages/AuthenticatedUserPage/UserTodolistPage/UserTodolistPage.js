import React from 'react';
import axios from 'axios'
import {DELETE_USER_TODOLIST, GET_USER_TODOLIST, POST_TODOLIST} from "../../../Api/Api";
import {Link} from 'react-router-dom';

export default function UserTodolistPage(){

    const [isLoading, setIsLoading] = React.useState(false);
    const [isDeleting, setIsDeleting] = React.useState(false);
    const [userTodolist, setUserTodolist] = React.useState();


    async function loadUserTodolist(){

        try {

            setIsLoading(true)

            const todolists = (await axios.get(GET_USER_TODOLIST)).data;
            setUserTodolist(todolists);

        } catch (e) {
            console.log(e);
        } finally {
            setIsLoading(false)
        }

    }

    async function deleteTodo(id){

        try {

            setIsDeleting(id)

            const response = (await axios.delete(`${DELETE_USER_TODOLIST}/${id}`)).data;

            loadUserTodolist();

        } catch (e) {
            console.log(e);
        } finally {
            setIsDeleting(undefined)
        }

    }

    React.useEffect(()=>{
        loadUserTodolist()
    }, [])

    return (
        <div>

            {
                !!userTodolist && (

                    <table className="table">

                        <thead>
                        <tr>
                            <th>Libellé</th>
                            <th>Nombre de tâches</th>
                            <th>Actions</th>
                        </tr>
                        </thead>

                        <tbody>

                        {
                            userTodolist.map(
                                todo => {
                                    return (
                                        <tr>
                                            <td>{todo.name}</td>
                                            <td>
                                                {
                                                    todo.list.filter(
                                                        listItem => !!listItem.checked
                                                        ).length
                                                } / {todo.list.length} tâches éffectués
                                            </td>
                                            <td>

                                                <Link to={`/todolist/form/${todo.id}`}>
                                                    <button className="btn btn-primary btn-sm">
                                                        Modifier
                                                    </button>
                                                </Link>


                                                <button onClick={()=>{
                                                    deleteTodo(todo.id);
                                                }} className="btn btn-danger btn-sm">

                                                    {
                                                        isDeleting == todo.id && (
                                                            'Is deleting...'
                                                        )
                                                    }

                                                    {
                                                        !isDeleting && (
                                                            'supprimer'
                                                        )
                                                    }

                                                </button>

                                            </td>
                                        </tr>
                                    )
                                }
                            )
                        }

                        {
                            userTodolist.length == 0 && (
                                <tr>
                                    <td colSpan={3}>
                                        <div className="py-3 text-center">
                                            Vous n'avez aucune todolist
                                        </div>
                                    </td>
                                </tr>
                            )
                        }

                        </tbody>

                    </table>
                )
            }


        </div>
    )
}