import React from 'react';
import TodoListForm from "../../../Component/TodoListForm";
import axios from 'axios'
import { useHistory } from 'react-router-dom';
import {POST_TODOLIST} from "../../../Api/Api";

export default function CreateTodolistPage(){

    const history = useHistory();

    const [isStoring, setIsStoring] = React.useState();

    async function storeTodoList({name, items}){

        try {

            setIsStoring(true)

            const result = await axios.post(
                POST_TODOLIST, {
                    name,
                    list: items
                }
            );

            history.push('/todolist');

        } catch (e) {
            console.log(e)
        } finally {
            setIsStoring(false)
        }

    }

    return (
        <div className={'px-5'}>

            <TodoListForm
                initialValues={{
                    name: '',
                    items: []
                }}
                onFormSubmit={(values)=>{
                storeTodoList(values)
            }} />

        </div>
    );
}