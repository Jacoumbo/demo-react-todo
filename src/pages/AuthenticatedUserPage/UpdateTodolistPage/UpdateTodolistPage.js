import React from 'react';
import TodoListForm from "../../../Component/TodoListForm";
import axios from 'axios'
import { useHistory, useParams, useLocation } from 'react-router-dom';
import {GET_TODO_ITEM, POST_TODOLIST, UPDATE_TODOLIST} from "../../../Api/Api";

export default function UpdateTodolistPage(){

    const history = useHistory();
    const {todoId} = useParams() || {};

    console.log('params', useParams())

    const [isUpdating, setIsUpdating] = React.useState();
    const [isLoading, setIsLoading] = React.useState();
    const [loadedTodo, setLoadedTodo] = React.useState();

    async function loadTodoInfos(){

        try {

            setIsLoading(true)
            const todo = (await axios.get(`${GET_TODO_ITEM}/${todoId}`)).data;

            setLoadedTodo(todo)

        } catch (e) {

            if(e.response && e.response.status == 404){
                alert('La todo demandé n\'existe pas');
            }

        } finally {
            setIsLoading(false)
        }

    }

    async function updateTodolist({name, items}){

        try {

            setIsUpdating(true)

            const result = await axios.post(
                `${UPDATE_TODOLIST}/${todoId}`, {
                    name,
                    list: items
                }
            );

            history.push('/todolist');

        } catch (e) {
            console.log(e)
        } finally {
            setIsUpdating(false)
        }

    }

    React.useEffect(()=>{
        loadTodoInfos()
    }, []);

    return (
        <div className={'px-5'}>

            {
                loadedTodo && (
                    <TodoListForm
                        initialValues={{
                            name: loadedTodo.name,
                            items: loadedTodo.list
                        }}
                        onFormSubmit={(values)=>{
                        updateTodolist(values)
                    }} />
                )
            }


        </div>
    );
}