import React from 'react';
import {Button} from 'react-bootstrap'
import axios from 'axios';
import {ME, REGISTER} from "../../Api/Api";
import {connectUser} from "../../stores/auth";
import {useDispatch, useSelector} from "react-redux";
import {Link} from 'react-router-dom';

export default function InscritionPage(){

    const dispatch = useDispatch();
    const [username, setUserName] = React.useState();
    const [password, setPassword] = React.useState();

    const [isRegisteringUser, setIsRegistering ] = React.useState(false);

    const [formErrors, setFormErrors] = React.useState({
        username: undefined,
        password: undefined,
    });

    async function doInscription(){

        const errors = {};

        if(username == undefined || username == '')
            errors.username = 'Vous devez saisir le champs username'

        if(password == undefined || password == '')
            errors.password = 'Vous devez saisir le champs password'

        setFormErrors(errors);

        if(!errors.username && !errors.password){

            try {


                setIsRegistering(true);
                //On éffectue notre action ici
                const response = await axios.post(
                    REGISTER, {
                        email: username,
                        password: password
                    }
                );


                const userInfos = await axios.get(ME,{
                    headers: {
                        Authorization: `Bearer ${response.data.token}`
                    }
                });

                dispatch(
                    connectUser(
                        userInfos,
                        response.data.token
                    )
                );

            } catch (e) {
                console.log('Error lors de l\'inscription');
            } finally {
                setIsRegistering(false)
            }

        }

        else {

        }

    }

    return (
        <div>

            <div className={'mb-6'}>
                Page d'inscription
            </div>

            <div>
                <div className="my-3">

                    <input value={username} onChange={(event)=>{setUserName(event.target.value)}} type="text" placeholder={'Nom d\'utilisateur'} className="form-control"/>

                    {
                        formErrors.username && (
                            <div className={'text-danger'}>{formErrors.username}</div>
                        )
                    }

                </div>
                <div className="my-3">
                    <input type={'password'} value={password} onChange={(event)=>{setPassword(event.target.value)}} placeholder={'Mot de passe'} className="form-control"/>

                    {
                        formErrors.password && (
                            <div className={'text-danger'}>{formErrors.password}</div>
                        )
                    }

                </div>
            </div>

            <Button onClick={()=>{
                doInscription();
            }}>Inscription</Button>


        </div>
    );
}
