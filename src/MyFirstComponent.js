import React from 'react';
import { Button } from 'react-bootstrap';

export default class MyFirstComponent extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            counter: 99,
            user: undefined,
            error: undefined,
        };
        this.onButtonClick.bind(this);

    }


    componentDidMount() {
        super.componentDidMount();
    }

    componentWillUnmount() {
        super.componentWillUnmount();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {

        super.componentDidUpdate(prevProps, prevState, snapshot);
    }

    onButtonClick(){
        this.setState({counter: 100});
    }

    render(){
        console.log('Method render called');
        return (
            <div>

                <div className="h1">{this.state.counter}</div>

                <Button onClick={this.onButtonClick}>
                    Mon premier boutton
                </Button>

            </div>
        )
    }
}