import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import MyFirstComponent from "./MyFirstComponent";
import 'bootstrap/dist/css/bootstrap.min.css';
import MyFirstFunctionnalComponent from "./MyFirstFunctionnalComponent";
import {Provider} from 'react-redux'
import store from './stores'
import {BrowserRouter} from 'react-router-dom'
import Routes from "./routes/Routes";
import {deconnectUser} from "./stores/auth";
import axios from 'axios';

axios.interceptors.request.use(req => {

    const userToken = store.getState().auth.token;

    if(userToken)
        req.headers.Authorization = `Bearer ${userToken}`;

    req.headers.Accept = `application/json`;

    return req;
});

axios.interceptors.response.use(response => {

        console.log('intercepted response',response);

        return response;
    },
    error => {

        if(error.response && error.response.status == 401){

            store.dispatch(deconnectUser());

            throw error;

            // window.location.href = '/login'
            return;
        }

        console.log('there are error in response',error.status, {...error});
        throw error;
    }
);


ReactDOM.render(
  <React.StrictMode>
      <Provider store={store}>
          <BrowserRouter>
              <Routes />
          </BrowserRouter>
      </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
