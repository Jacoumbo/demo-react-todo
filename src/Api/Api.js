const BASE_HOST_NAME = `http://localhost:8000`

const BASE_URL = `${BASE_HOST_NAME}/api/client`


export const REGISTER = `${BASE_URL}/auth/register`
export const ME = `${BASE_URL}/auth/me`
export const LOGIN = `${BASE_URL}/auth/login`


export const POST_TODOLIST = `${BASE_URL}/todos`
export const UPDATE_TODOLIST = `${BASE_URL}/todos`
export const GET_USER_TODOLIST = `${BASE_URL}/todos`
export const GET_TODO_ITEM = `${BASE_URL}/todos`
export const DELETE_USER_TODOLIST = `${BASE_URL}/todos`