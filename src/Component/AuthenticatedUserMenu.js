import React from 'react';
import {Link} from "react-router-dom";
import {useDispatch} from "react-redux";
import {deconnectUser} from "../stores/auth";


export default function AuthenticatedUserMenu(){

    const dispatch = useDispatch();

    return (
        <ul>
            <li>
                <Link to={'/profils'}>
                    <span>Profil utilisateur</span>
                </Link>
            </li>
            <li>
                <Link to={'/todolist'}>
                    <span>Liste des Todolist</span>
                </Link>
            </li>
            <li>
                <Link to={'/todolist/form'}>
                    <span>Nouveau todolist</span>
                </Link>
            </li>
            <li>
                <a onClick={()=>{
                    dispatch(deconnectUser());
                }} href="#">
                    <span>Déconnexion</span>
                </a>
            </li>
        </ul>
    );
}