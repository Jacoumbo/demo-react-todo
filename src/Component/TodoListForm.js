import React from 'react';
import {Formik, useFormik} from 'formik'
import {Button} from 'react-bootstrap'

function TodolistItem(props){

    const {formikHelpers, id, item, onDeleteItem} = props;

    return (
        <div className={'row py-1 my-3'}>

            <div className="col-10 small font-weight-bold">
                {item.label}
            </div>

            <div className="col-2">

                <input
                    checked={item.checked}
                    onChange={(event)=>{

                    formikHelpers.setFieldValue('items',
                        formikHelpers.values.items.map(
                            (todoItem, index) => {

                                if(index == id){
                                    return {
                                        ...todoItem,
                                        checked: event.target.checked
                                    }
                                }

                                return todoItem;
                            }
                        )
                    );

                }} type={'checkbox'} />

                <button onClick={()=>{
                    onDeleteItem();
                }} className={'btn btn-sm btn-danger'}>
                    del.
                </button>

            </div>

            <div className="col-12 mb-2">
                <hr />
            </div>

        </div>
    )
}

export default function TodoListForm({onFormSubmit, initialValues}){

    const [addInputValue, setAddInputValue] = React.useState('');

    const formik = useFormik({
        initialValues,
        onSubmit(values, formikHelpers){
            if(onFormSubmit)
                onFormSubmit(values);
        }
    });

    console.log('Formik values', formik.values);

    function addTodoListItem(){

        formik.setFieldValue(
            'items', [
                ...formik.values.items,
                {
                    label: addInputValue,
                    checked: false,
                }
            ]
        );

        setAddInputValue('');

    }

    return (
        <fieldset>

            <div className={'row'}>

                <div className="col-12 mb-">

                    <div>Nom identifiant de la todolist</div>

                    <input onChange={(event)=>{
                        formik.setFieldValue('name', event.target.value)
                    }} value={formik.values.name} type="text" className={'form-control'} placeholder={'Todolist Name ID'}/>

                    <hr />

                </div>

                <div className="col mb-5">
                    <input placeholder={'Nom du todo item'} type="text" value={addInputValue} className={'form-control'} onChange={(event) => {
                        setAddInputValue(event.target.value)
                    }}/>
                </div>

                <div className={'col-auto'}>
                    <Button onClick={()=>{
                        addTodoListItem()
                    }}>
                        Ajouter un item
                    </Button>
                </div>

                {
                    formik.values.items.map(
                        (todoItem, index) => (
                            <TodolistItem
                                key={index}
                                id={index}
                                onDeleteItem={()=>{
                                    formik.setFieldValue(
                                        'items',
                                        formik.values.items.filter(
                                            (item, itemIndex) => {
                                                return itemIndex != index
                                            }
                                        )
                                    );
                                }}
                                item={todoItem} formikHelpers={formik} />
                        )
                    )
                }


                <Button onClick={()=>{
                    formik.submitForm();
                }}>
                    Soumettre mon formulaire
                </Button>

            </div>

        </fieldset>
    )
}