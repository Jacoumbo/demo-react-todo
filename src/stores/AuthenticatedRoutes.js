import React from 'react';
import {Switch, Redirect, Route} from 'react-router-dom';
import DashboardPage from "../pages/AuthenticatedUserPage/Dashboard/DashboardPage";
import UserProfilePage from "../pages/AuthenticatedUserPage/UserProfilePage/UserProfilePage";
import UserTodolistPage from "../pages/AuthenticatedUserPage/UserTodolistPage/UserTodolistPage";
import AuthenticatedUserMenu from "../Component/AuthenticatedUserMenu";
import CreateTodolistPage from "../pages/AuthenticatedUserPage/CreateTodolistPage/CreateTodolistPage";
import UpdateTodolistPage from "../pages/AuthenticatedUserPage/UpdateTodolistPage/UpdateTodolistPage";

export default function AuthenticatedRoutes(){

    return (

        <div>

            <AuthenticatedUserMenu />

            <Switch>

                <DashboardPage path={'/dashboard'} />

                <CreateTodolistPage exact path={'/todolist/form'} />
                <Route component={UpdateTodolistPage} exact path={'/todolist/form/:todoId'} />
                <UserProfilePage exact path={'/profils'} />
                <UserTodolistPage exact path={'/todolist'} />

                <Redirect to={'/dashboard'} />

            </Switch>
            
        </div>

    );
}