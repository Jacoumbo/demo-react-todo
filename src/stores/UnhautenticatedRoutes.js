import React from 'react';
import {Redirect, Switch} from "react-router-dom";
import AuthenticatedRoutes from "./AuthenticatedRoutes";
import InscriptionPage from "../pages/Inscription/InscriptionPage";
import ConnexionPage from "../pages/Connexion/ConnexionPage";


export default function UnhautenticatedRoutes(props) {

    return (
        <Switch>

            <InscriptionPage exact path={'/inscription'}/>
            <ConnexionPage exact path={'/connexion'}/>

            <Redirect to={'/connexion'}/>

        </Switch>

    )
}