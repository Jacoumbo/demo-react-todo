import {applyMiddleware, combineReducers, createStore} from "redux";
import authStore from './auth';
import thunk from "redux-thunk";
import {persistReducer, persistStore} from "redux-persist";
import storage from 'redux-persist/lib/storage'
import {composeWithDevTools} from "redux-devtools-extension";

const combinedReducers = combineReducers({
    auth: authStore,
});


const persistConfig = {
    key: 'root',
    storage : storage,
    whitelist: ['auth'],
    timeout: null
};

const persistedReducer = persistReducer(persistConfig,combinedReducers);

const store = createStore(persistedReducer, composeWithDevTools(applyMiddleware(thunk)));

const persistedStore = persistStore(store)

// persistedStore.purge();

export default store;