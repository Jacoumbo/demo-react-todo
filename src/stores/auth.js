//0è partie - L'état initial
const initialState = {
    user: undefined,
    token: undefined,
    nombreTentativeConnexion: 1
};


//1ere partie - Les types d'actions
export const CONNECT_USER = 'authStore/CONNECT_USER'
export const DECONNECT_USER = 'authStore/DECONNECT_USER'

//2è partie - Les actions creator
export function connectUser(user, token){
    return {
        type: CONNECT_USER,
        payload: {
            user,
            token
        }
    }
}

export function deconnectUser(){
    return async function(dispatch, prevState){
        dispatch({
            type: DECONNECT_USER
        });
    }
}



//3è partie - Le reducer
export default function authStore(prevState = initialState, action){

    console.log('action reçu', prevState, action);

    /*
    *   action = {
    *
    *       type: 'apzeiazepazoi',
    *       payload: {}
    *
    *   }
    *
     */

    const payload = action.payload;

    switch (action.type) {

        case CONNECT_USER:
            return {
                ...prevState,
                user: payload.user,
                token: payload.token
            };

        case DECONNECT_USER:
            return {
                ...prevState,
                token: undefined,
                user: undefined
            }

        default:
            return prevState;
    }

}