import React, {useEffect} from 'react';
import { Button } from 'react-bootstrap';
import InscriptionPage from './pages/Inscription/InscriptionPage'
import ConnexionPage from "./pages/Connexion/ConnexionPage";
import {connectUser} from "./stores/auth";
import {useSelector, useDispatch} from "react-redux";

export default function MyFirstFunctionnalComponent(){

    const [counter, setCounter] = React.useState(0);
    const [inputValue, setInputValue] = React.useState();
    const userState = React.useState(undefined);

    const [currentTab, setCurrentTab] = React.useState('tab1')

    function onButtonClick(){

        if(currentTab == 'tab1')
            setCurrentTab('tab2')

        else
            setCurrentTab('tab1')

    }


    return (
        <div className={'px-6 py-6'}>
            <ConnexionPage />
        </div>
    );
}